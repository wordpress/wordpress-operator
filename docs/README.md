# Docs
## Deploy locally with OLM
### Deploy changes to staging (OLM)

Prior to deploy changes to the staging cluster, let's generate the bundle to verify that indeed. We need to create a catalog index composed of bundle(s) that will be eventually managed by the Operator Lifecycle Manager.

Setup the Operator SDK:

```bash
# Assuming your operator repository is under D:\wordpress\wordpress-operator
docker run -it --rm -v "D:\wordpress\wordpress-operator:/wordpress-operator" -v /var/run/docker.sock:/var/run/docker.sock -w /wordpress-operator docker:19.03.1

apk add make curl gnupg
# Add glibc, requisite for opm
wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.33-r0/glibc-2.33-r0.apk
apk add glibc-2.33-r0.apk

export OPERATOR_SDK_VERSION=1.37.0
export ARCH=$(case $(uname -m) in x86_64) echo -n amd64 ;; aarch64) echo -n arm64 ;; *) echo -n $(uname -m) ;; esac)
export OS=$(uname | awk '{print tolower($0)}')
export OPERATOR_SDK_DL_URL=https://github.com/operator-framework/operator-sdk/releases/download/v${OPERATOR_SDK_VERSION}
curl -LO ${OPERATOR_SDK_DL_URL}/operator-sdk_${OS}_${ARCH}
gpg --keyserver keyserver.ubuntu.com --recv-keys 052996E2A20B5C7E
curl -LO ${OPERATOR_SDK_DL_URL}/checksums.txt
curl -LO ${OPERATOR_SDK_DL_URL}/checksums.txt.asc
gpg -u "Operator SDK (release) <cncf-operator-sdk@cncf.io>" --verify checksums.txt.asc
grep operator-sdk_${OS}_${ARCH} checksums.txt | sha256sum -c -
chmod +x operator-sdk_${OS}_${ARCH} && mv operator-sdk_${OS}_${ARCH} /usr/local/bin/operator-sdk
```

And generate the new bundle:

```bash
export VERSION=0.0.<X> # align with the values of the Update operator version procedure above.
export IMAGE_TAG_BASE="registry.cern.ch/wordpress/wordpress-operator/wordpress-operator"
export IMG="${IMAGE_TAG_BASE}-controller:${VERSION}"
export CATALOG_IMG=${IMAGE_TAG_BASE}-catalog:dev
export CATALOG_BASE_IMG=${IMAGE_TAG_BASE}-catalog:dev
export BUNDLE_IMG=${IMAGE_TAG_BASE}-bundle:v${VERSION}
export BUNDLE_CHANNELS="--channels='stable'"
export BUNDLE_DEFAULT_CHANNEL="--default-channel='stable'"
export BUNDLE_METADATA_OPTS="${BUNDLE_CHANNELS} ${BUNDLE_DEFAULT_CHANNEL}"
export BUNDLE_IMGS=${BUNDLE_IMG}

# Login into registry.cern.ch. Pick the auth token from the CI/CD variables
export DOCKER_AUTH_CONFIG="{\"auths\":{\"registry.cern.ch\":{\"auth\":\"xxx\"}}}"
mkdir -p ~/.docker/ && echo "${DOCKER_AUTH_CONFIG}" > ~/.docker/config.json

# Generate controller image and push it.
# make docker-build docker-push
# Generate, build and push the bundle manifests.
# make bundle bundle-build bundle-push

# Generate controller image, build and push the bundle manifests altogether.
make docker-build docker-push bundle bundle-build bundle-push
```

Once done, push all changes to a new branch and open a Merge Request. From this Merge Request, you can deploy a new version to the [OKD4 App Catalogue staging cluster](https://app-cat-stg.cern.ch) by triggering the `deploy-bundle-staging` job. This will point the catalog to `catalog:staging`. Once this job is triggered, the upgrade of the operator under the staging cluster does not happen instantaneously, but it takes up to 30 mins.

### Deploy changes to production (OLM)

Same as in the above staging procedure, we need to create a catalog index composed of bundle(s) that will be eventually managed by the Operator Lifecycle Manager.

#### Deploy the first version of the operator

For the first time the operator is going to be deployed, we need to generate, build and push the bundle and the catalog index that contains the first bundle:

Set the version and **unset** the `CATALOG_BASE_IMG` environment variable:

For staging clusters:

```bash
# Set the environment variables needed for staging
export VERSION=0.0.<X>
export IMAGE_TAG_BASE="registry.cern.ch/wordpress/wordpress-operator/wordpress-operator"
export IMG="${IMAGE_TAG_BASE}-controller:${VERSION}"
# Since this is the first version, CATALOG_BASE_IMG env variable is not needed.
unset CATALOG_BASE_IMG
# However, for production or staging we need to explicitly set a dedicated catalog
export CATALOG_IMG=${IMAGE_TAG_BASE}-catalog:staging
export BUNDLE_IMG=${IMAGE_TAG_BASE}-bundle:v${VERSION}
export BUNDLE_CHANNELS="--channels='stable'"
export BUNDLE_DEFAULT_CHANNEL="--default-channel='stable'"
export BUNDLE_METADATA_OPTS="${BUNDLE_CHANNELS} ${BUNDLE_DEFAULT_CHANNEL}"
export BUNDLE_IMGS=${BUNDLE_IMG}
```

For production clusters:

```bash
# Set the environment variables needed for staging
export VERSION=0.0.<X>
export IMAGE_TAG_BASE="registry.cern.ch/wordpress/wordpress-operator/wordpress-operator"
export IMG="${IMAGE_TAG_BASE}-controller:${VERSION}"
# Since this is the first version, CATALOG_BASE_IMG env variable is not needed.
unset CATALOG_BASE_IMG
# However, for production or staging we need to explicitly set a dedicated catalog
export CATALOG_IMG=${IMAGE_TAG_BASE}-catalog:latest
export BUNDLE_IMG=${IMAGE_TAG_BASE}-bundle:v${VERSION}
export BUNDLE_CHANNELS="--channels='stable'"
export BUNDLE_DEFAULT_CHANNEL="--default-channel='stable'"
export BUNDLE_METADATA_OPTS="${BUNDLE_CHANNELS} ${BUNDLE_DEFAULT_CHANNEL}"
export BUNDLE_IMGS=${BUNDLE_IMG}
```

Then, ensure that `metadata.annotations.olm.skipRange` under `config\manifests\bases\wordpress-operator.clusterserviceversion.yaml` is removed. There is no version to be replaced in the first version.

And then proceed to generate, build and push the controller, the bundle and the catalog to the registry.

```bash
make docker-build docker-push
make bundle bundle-build bundle-push catalog-build catalog-push
```

In order to let OKD cluster knows about the generated catalog, and hence be able to install the WordPress operator, some resources must be provided to the OKD cluster. This is automatically done thanks to the [https://gitlab.cern.ch/paas-tools/okd4-install/-/tree/master/chart/charts/operators-catalogue](https://gitlab.cern.ch/paas-tools/okd4-install/-/tree/master/chart/charts/operators-catalogue) component.

A `CatalogSource`:

```yaml
apiVersion: operators.coreos.com/v1alpha1
kind: CatalogSource
metadata:
  name: wordpress-operator
  namespace: openshift-cern-wordpress-operator
spec:
  sourceType: grpc
  image: registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-catalog:latest
  displayName: WordPress Operator Catalog
  publisher: Openshift Admins
  updateStrategy:
    registryPoll: 
      interval: 10m
```

Note the `image: registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-catalog:latest` element and the `interval: 10m`. This means that the catalog source operator will be pulling changes from our catalog every `10m`.

A `Subscription` (for both staging and production clusters, we will use `channel: stable`). Note the `installPlanApproval: Manual` element. This means that we need to approve the `InstallPlan` of a newer version of the operator (including the first one). This can be valuable for production clusters, since we do not want the new operator to be deployed automatically, but on demand. For dev/staging clusters, we can consider using `installPlanApproval: Automatic`.

```yaml
apiVersion: operators.coreos.com/v1alpha1
kind: Subscription
metadata:
  name: wordpress-operator
  namespace: openshift-cern-wordpress-operator
spec:
  channel: stable
  installPlanApproval: Automatic
  name: wordpress-operator
  source: wordpress-operator
  sourceNamespace: openshift-cern-wordpress-operator
```

An `OperatorGroup`:

```yaml
apiVersion: operators.coreos.com/v1
kind: OperatorGroup
metadata:
  annotations:
    olm.providedAPIs: Wordpress.v1.wordpress.webservices.cern.ch
  name: wordpress-operator
  namespace: openshift-cern-wordpress-operator
spec: {}
```

After some seconds, we can navigate to `Administrator` > `Operators` > `Installed Operators` window, click under `WordPress` operator. In the new window, change to the `Subscription` tab and approve the `InstallPlan` if we have selected `installPlanApproval: Automatic`.

And that's it, our WordPress operator should soon appear under the `Administrator` > `Operators` > `Installed Operators` window.

#### Deploy consecutive versions of the operator

For consecutive times, ensure that:

Under `.gitlab-ci.yaml`, set the new version.

```yaml
variables:
  VERSION: 0.0.2 # new version, previously 0.0.1  
```

Add/Update `metadata.annotations.olm.skipRange` element under `config\manifests\bases\wordpress-operator.clusterserviceversion.yaml`, by setting `olm.skipRange: <0.0.X`, being `X` the future version we want to deploy in an OKD cluster. For this use case, set `olm.skipRange: <0.0.2` (since we are going to deploy version `0.0.2`, and `0.0.1` was previously deployed).

Update the version of the controller manager under `config\manager\kustomization.yaml`.

Once all changes deployed to the staging cluster has been verified, the procedure to deploy the new version of the operator in production is as follows:

- To merge the Merge request first.
- To generate a new `Tag`. This tag will create a new job (manual) that will allow us to deploy changes to the production cluster (catalog will point to `catalog:latest`).

At this point, since we have previously configured the `CatalogSource` image (in the OKD configuration) to point to the tag `registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-catalog:latest`, after `spec.updateStrategy.registryPoll.interval` minutes, our new version of the operator will be ready to be automatically updated (`installPlanApproval: Automatic`) or to suggest us to upgrade it upon click (`installPlanApproval: Manual`).


## WordPress Operator Development

Below you will find the tools needed for development purposes.

### Pre-requisites

In order to iteratively build and debug new changes, setup the the following. This also assumes you have docker up and running in your local pc:

```bash
# Assuming your operator repository is under D:\wordpress\wordpress-operator
docker run -it --rm -v "D:\wordpress\wordpress-operator:/wordpress-operator" -v /var/run/docker.sock:/var/run/docker.sock -w /wordpress-operator docker:19.03.1

apk add make curl gnupg
# Add glibc, requisite for opm
wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.33-r0/glibc-2.33-r0.apk
apk add glibc-2.33-r0.apk

export OPERATOR_SDK_VERSION=1.37.0
export ARCH=$(case $(uname -m) in x86_64) echo -n amd64 ;; aarch64) echo -n arm64 ;; *) echo -n $(uname -m) ;; esac)
export OS=$(uname | awk '{print tolower($0)}')
export OPERATOR_SDK_DL_URL=https://github.com/operator-framework/operator-sdk/releases/download/v${OPERATOR_SDK_VERSION}
curl -LO ${OPERATOR_SDK_DL_URL}/operator-sdk_${OS}_${ARCH}
gpg --keyserver keyserver.ubuntu.com --recv-keys 052996E2A20B5C7E
curl -LO ${OPERATOR_SDK_DL_URL}/checksums.txt
curl -LO ${OPERATOR_SDK_DL_URL}/checksums.txt.asc
gpg -u "Operator SDK (release) <cncf-operator-sdk@cncf.io>" --verify checksums.txt.asc
grep operator-sdk_${OS}_${ARCH} checksums.txt | sha256sum -c -
chmod +x operator-sdk_${OS}_${ARCH} && mv operator-sdk_${OS}_${ARCH} /usr/local/bin/operator-sdk
```

### Develop a new version

First version of the operator is a special case, so we will deal it separately. Let's assume we are in the version `0.0.1` already deployed, and we want to change to version `0.0.2`. First, let's deploy the new version in the staging clusters. For this purpose, we will use the channel `stable` for both staging and production clusters. We will just add the bundles to the corresponding catalog which is pointing to `catalog:staging` for staging clusters, and `catalog:latest`, which is pointing to production clusters.

For this change, remember to update the `metadata.annotations.olm.skipRange` value under `config\manifests\bases\wordpress-operator.clusterserviceversion.yaml`, pointing to the previous `VERSION` (e.g., if now we are going to install `VERSION=0.0.2`, `metadata.annotations.olm.skipRange` must be `olm.skipRange: <0.0.2`).

When using the `.gitlab-ci.yaml`, do not forget to update also the `VERSION` value.

With the previous tool, let's generate the new bundle:

```bash
export VERSION=0.0.<X>
export IMAGE_TAG_BASE="registry.cern.ch/wordpress/wordpress-operator/wordpress-operator"
export IMG="${IMAGE_TAG_BASE}-controller:${VERSION}"
export CATALOG_IMG=${IMAGE_TAG_BASE}-catalog:dev
export CATALOG_BASE_IMG=${IMAGE_TAG_BASE}-catalog:dev
export BUNDLE_IMG=${IMAGE_TAG_BASE}-bundle:v${VERSION}
export BUNDLE_CHANNELS="--channels='stable'"
export BUNDLE_DEFAULT_CHANNEL="--default-channel='stable'"
export BUNDLE_METADATA_OPTS="${BUNDLE_CHANNELS} ${BUNDLE_DEFAULT_CHANNEL}"
export BUNDLE_IMGS=${BUNDLE_IMG}

# Login into registry.cern.ch
export DOCKER_AUTH_CONFIG="{\"auths\":{\"registry.cern.ch\":{\"auth\":\"xxx\"}}}"
mkdir -p ~/.docker/ && echo "${DOCKER_AUTH_CONFIG}" > ~/.docker/config.json

# Generate controller image and push it.
make docker-build docker-push
# Generate, build and push the bundle manifests.
make bundle bundle-build bundle-push

# Or altogether
make docker-build docker-push bundle bundle-build bundle-push
```

At this point, we will be able to test our bundle in a dev cluster. Note that is not recommended to perform any change under the generated `bundle` folder. Everything is auto-generated from the `config/` folder:

```bash
# Connect to a development cluster
# First download oc
curl -L https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable-4.7/openshift-client-linux.tar.gz | tar xvz -C /tmp && mv /tmp/oc /usr/local/bin
# You can also `export KUBECONFIG=<path to existing kubeconfig file>`
oc login ...
# Create a namespace where your operator and all associated resources will land
# (`CatalogSource`, `Subscription`, `OperatorGroup`, etc.) in your local cluster:
oc create namespace openshift-cern-wordpress-operator

# create configuration for wordpress-operator
# (set ACME_SERVER to empty string to generate self-signed certificates
# or 'https://acme-staging-v02.api.letsencrypt.org/directory' for LE staging)
oc create configmap wordpress-operator-configuration -n openshift-cern-wordpress-operator --from-literal=ACME_SERVER='' --from-literal=WILDCARD_DELEGATED_DOMAINS_REGEX='^([a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*\.)(webtest\.cern\.ch|apptest\.cern\.ch){1}$'

# Run scoreboad tests to check whether you are properly using spec/status Descriptors, etc. You should pass all tests.
operator-sdk scorecard registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-bundle:v${VERSION}

# Now, we can test the bundle on an OKD4 cluster.
operator-sdk run bundle registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-bundle:v${VERSION} -n openshift-cern-wordpress-operator
# sample output
# INFO[0008] Successfully created registry pod: h-wordpress-wordpress-operator-wordpress-operator-bundle-v0-0-2
# INFO[0008] Created CatalogSource: wordpress-operator-catalog
# INFO[0008] OperatorGroup "operator-sdk-og" created
# INFO[0008] Created Subscription: wordpress-operator-v0-0-2-sub
# INFO[0012] Approved InstallPlan install-jslw9 for the Subscription: wordpress-operator-v0-0-2-sub
# INFO[0012] Waiting for ClusterServiceVersion "openshift-cern-wordpress-operator/wordpress-operator.v0.0.2" to reach 'Succeeded' phase
# INFO[0012]   Waiting for ClusterServiceVersion "openshift-cern-wordpress-operator/wordpress-operator.v0.0.2" to appear
# INFO[0023]   Found ClusterServiceVersion "openshift-cern-wordpress-operator/wordpress-operator.v0.0.2" phase: Pending
# INFO[0024]   Found ClusterServiceVersion "openshift-cern-wordpress-operator/wordpress-operator.v0.0.2" phase: Installing
# INFO[0033]   Found ClusterServiceVersion "openshift-cern-wordpress-operator/wordpress-operator.v0.0.2" phase: Succeeded
# INFO[0033] OLM has successfully installed "wordpress-operator.v0.0.2"
```
NB: with some operator-sdk versions the OPM bundle pod may not start due to permission error, workaround:
add `--index-image=quay.io/operator-framework/opm:v1.23.0` to `operator-sdk run bundle`
(cf. <https://github.com/operator-framework/operator-registry/issues/984#issuecomment-1175091401>)

If for some reason we are experiencing issues with our helm manifests, we can consider:

```bash
# wordpress-operator/
# Login into the dev cluster
# export KUBECONFIG=...

# Note that in helm is not included in the Makefile. You would need to download it separately if needed.
# Check https://helm.sh/docs/intro/install/
helm template helm-charts/wordpress

# To just run the controller manager, execute:
make run
```

To clean up our tests, consider:

```bash
# login into the cluster as cluster admin
cd wordpress-operator/

operator-sdk cleanup wordpress-operator --delete-all -n openshift-cern-wordpress-operator

# In case you are manually adding CatalogSource, Subscription, etc.
oc delete CatalogSource --all -n openshift-cern-wordpress-operator
oc delete OperatorGroup --all -n openshift-cern-wordpress-operator
oc delete Subscription --all -n openshift-cern-wordpress-operator
oc get csv -A
oc delete csv/wordpress-operator.v0.0.<X> -n openshift-cern-wordpress-operator

# To delete the CRD
kustomize build config/crd | kubectl delete -f -
```

If we are happy with our results, next steps are:

- Ensure that `metadata.annotations.olm.skipRange` under `config\manifests\bases\wordpress-operator.clusterserviceversion.yaml` is less than the current version. If it's the `0.0.1` first version, then remove the `metadata.annotations.olm.skipRange` element.
- Prior to commit changes, do not forget to update the `.gitlab-ci.yaml` from:

  ```yaml
  variables:
    VERSION: 0.0.1
    ...
  ```

  To:

  ```yaml
  variables:
    VERSION: 0.0.2
    ...
  ```

- commit the changes and let the CI do the work for us, i.e., to build and push both the bundle and the catalog, this last containing the bundle we have generated.

Once pushed, the CI will trigger the build of the bundle according to the variables we pass under `.gitlab-ci.yaml`, and after building, we manually will build and push the catalog with the new version.

#### Simulate catalog provision in dev

If we want to simulate how the new version will behave once we deployed, we can create a `catalog:dev` and play with it.

We just need to push the precedent bundles, and finally push the bundle we are working on.

```bash
# Assume we are working on version 0.0.3, that will substitute 0.0.2
./bin/opm index add --container-tool docker --mode semver --tag registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-catalog:dev --bundles registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-bundle:v0.0.1,registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-bundle:v0.0.2

# Push the catalog
make catalog-push
```

Now create the corresponding `CatalogSource`, `Subscription` and `OperatorGroup` in case they are not created in your dev environment:

```yaml
apiVersion: operators.coreos.com/v1alpha1
kind: CatalogSource
metadata:
  name: wordpress-operator
  namespace: openshift-cern-wordpress-operator
spec:
  sourceType: grpc
  image: registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-catalog:dev
  displayName: WordPress Operator Catalog
  publisher: Openshift Admins
  updateStrategy:
    registryPoll:
      interval: 5m

---

apiVersion: operators.coreos.com/v1alpha1
kind: Subscription
metadata:
  name: wordpress-operator
  namespace: openshift-cern-wordpress-operator
spec:
  channel: stable
  installPlanApproval: Automatic
  name: wordpress-operator
  source: wordpress-operator
  sourceNamespace: openshift-cern-wordpress-operator

---

apiVersion: operators.coreos.com/v1
kind: OperatorGroup
metadata:
  annotations:
    olm.providedAPIs: Wordpress.v1.wordpress.webservices.cern.ch
  name: wordpress-operator
  namespace: openshift-cern-wordpress-operator
spec: {}

```

And finally append the new bundle to see how the installation will behave.

```bash
# Now append the new bundle for version 0.0.3
./bin/opm index add --container-tool docker --mode semver --tag registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-catalog:dev --bundles registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-bundle:v0.0.3 --from-index registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-catalog:dev

# Push the catalog
make catalog-push
```

If you now navigate to your dev OKD4 cluster > `Administration` tab > `Operators` > `Installed Operators` you will see the WordPress Operator installed.

Create a WordPress site to verify it works properly:

```bash
# cd wordpress-operator/
# export KUBECONFIG=...

echo "
apiVersion: v1
kind: Namespace
metadata:
  name: my-awesome-wordpress
" | oc apply -f -


echo "
apiVersion: wordpress.webservices.cern.ch/v1
kind: Wordpress
metadata:
  name: my-wordpress
  namespace: my-awesome-wordpress
spec:
  wordpress:
    # Adequate hostname to the OKD4 dev cluster.
    hostname: 'my-wordpress-my-awesome-wordpress.<cluster-name>.cern.ch'
    adminEmail: 'ismael.posada.trobo@cern.ch'
    replicaCount: 1
    resources:
      limits:
        cpu: '1'
        memory: '1Gi'
      requests:
        cpu: '100m'
        memory: '140Mi'
  database:
    replicaCount: 1
    resources:
      limits:
        cpu: '1'
        memory: '1Gi'
      requests:
        cpu: '6m'
        memory: '400Mi'
" | oc apply -f -
```

And check the resources:

```bash
oc get wordpress -n  my-wordpress
# NAME               AGE
# wordpress-sample   12s

oc get all -n my-wordpress
# NAME                                          READY   STATUS    RESTARTS   AGE
# pod/my-wordpress-database-67684dd489-gznk7    1/1     Running   0          24m
# pod/my-wordpress-wordpress-54b9cf575c-nzcd7   1/1     Running   0          24m

# NAME                             TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
# service/my-wordpress-database    ClusterIP   172.30.247.42   <none>        3306/TCP   24m
# service/my-wordpress-wordpress   ClusterIP   172.30.142.45   <none>        8080/TCP   24m

# NAME                                     READY   UP-TO-DATE   AVAILABLE   AGE
# deployment.apps/my-wordpress-database    1/1     1            1           24m
# deployment.apps/my-wordpress-wordpress   1/1     1            1           24m

# NAME                                                DESIRED   CURRENT   READY   AGE
# replicaset.apps/my-wordpress-database-67684dd489    1         1         1       24m
# replicaset.apps/my-wordpress-wordpress-54b9cf575c   1         1         1       24m

# NAME                                   SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
# cronjob.batch/my-wordpress-wordpress   0 3 * * *   False     0        <none>          24m

# NAME                                              HOST/PORT                                                    PATH   SERVICES                 PORT       TERMINATION     WILDCARD
# route.route.openshift.io/my-wordpress-wordpress   my-wordpress-test-iposadat.okd-iposadat.cern.ch ... 1 more          my-wordpress-wordpress   8080-tcp   edge/Redirect   None
```

Whether we want to try more changes iteratively, either we increase the version and use:

```bash
operator-sdk run bundle-upgrade registry.cern.ch/wordpress/wordpress-operator/wordpress-operator-bundle:v${VERSION} -n openshift-cern-wordpress-operator
```

Or we just clean up everything and re-run it again:

```bash
operator-sdk cleanup wordpress-operator -n openshift-cern-wordpress-operator
```

For reference:

- Base Operator-CI: [https://gitlab.cern.ch/paas-tools/infrastructure-ci/-/blob/master/operator-ci-templates/DockerImages.gitlab-ci.yml](https://gitlab.cern.ch/paas-tools/infrastructure-ci/-/blob/master/operator-ci-templates/DockerImages.gitlab-ci.yml)
- [Official Operator SDK documentation about upgrading your operator](https://sdk.operatorframework.io/docs/olm-integration/generation/#upgrade-your-operator)
