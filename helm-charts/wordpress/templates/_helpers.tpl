{{/*
Create a fully qualified app name for both wordpress and database.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "wordpress.fullname" -}}
{{- $name := .Values.wordpress.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "database.fullname" -}}
{{- $name := .Values.database.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "per-hostname-resource.fullname" -}}
{{/*
    expecting a list of 2 params: the document root and the hostname
    TODO: hostname will look like aa.bb.cc and ideally we should truncate the resulting first segment <Release.Name + aa> at 63 chars
 */}}
{{- $ := index . 0 }}
{{- $hostname := index . 1 }}
{{- printf "%s-%s" $.Release.Name $hostname }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "app.fullname" -}}
{{- $name := .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}